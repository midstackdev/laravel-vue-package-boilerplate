<?php

namespace Qodehub\TicketingApp\Console;

use Illuminate\Console\Command;

class AssetsCommand extends Command
{
    protected $signature = 'qodehub:tickets.assets';

    protected $description = 'Update support-ticket-system assets' ;

    public function handle()
    {
        if (is_null(config('qodehub.tickets'))) {
            return $this->warn('Please publish the config file by running \'php artisan vendor:publish --tag=qodehub.tickets.config\'');
        }
        
        $this->call('vendor:publish', [
            '--tag' => 'qodehub.tickets.assets',
            '--force' => true,
        ]);

        // $this->call('vendor:publish', [
        //     '--tag' => 'views',
        //     '--force' => true,
        // ]);
    }
}