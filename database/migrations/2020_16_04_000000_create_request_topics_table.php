<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestTopicsTable extends Migration
{
    public function up()
    {   
        /**
         * Run the migrations.
         *
         * @return void
         */
         Schema::create('request_topics', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->unique()->index();
            $table->string('title')->index();
            $table->timestamps();
            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::dropIfExists('request_topics');
    }
}